package hospital;

import java.util.Date;
import java.util.*;

public class HospitalAggCompSimulation {

    public static void main(String[] args) {
        
        WristBands virusBand = new WristBands(999, "Viral Issue");
        WristBands cardiacBand = new WristBands(888, "Heart Issue");
        WristBands seizureBand = new WristBands(777, "Seizure Issue");
        WristBands asthmaBand = new WristBands(555, "Asthma Issue");

        List<WristBands> lowRisk  = new ArrayList<>();
        lowRisk.add(virusBand);
        
        List<WristBands> seriousRisk  = new ArrayList<>();
        seriousRisk.add(cardiacBand);
        seriousRisk.add(seizureBand);

        List<WristBands> fatalRisk  = new ArrayList<>();
        fatalRisk.add(virusBand);
        fatalRisk.add(cardiacBand);
        fatalRisk.add(asthmaBand);
        
        Patient patient1 = new Patient("Leo Satch", 
                new Date(1995, 07, 19, 12, 30, 0), "Doctor Simmons", lowRisk);
        Patient patient2 = new Patient("Carol Dims", 
                new Date(1984, 05, 15, 4, 10, 0), 
                "Doctor Leonard", seriousRisk);
        Patient patient3 = new Patient("Fletcher Adams", 
                new Date(1979, 03, 22, 8, 15, 0), "Doctor Sabel", fatalRisk);
        Patient patient4 = new Patient("Harry Kitts", 
                new Date(2003, 07, 19, 12, 30, 0), "Doctor Franklin", lowRisk);
        Patient patient5 = new Patient("Sarah Miles", 
                new Date(2009, 05, 15, 4, 10, 0), "Doctor Sasha", lowRisk);
        Patient patient6 = new Patient("Andy Pacton", 
                new Date(2015, 03, 22, 8, 15, 0), "Doctor Pino", seriousRisk);
        Patient patient7 = new Patient("Doug Hiller", 
                new Date(1997, 07, 19, 12, 30, 0), "Doctor Rougemont", 
                seriousRisk);
        Patient patient8 = new Patient("Samantha Cruise", 
                new Date(1982, 05, 15, 4, 10, 0), "Doctor Clyde", fatalRisk);
        Patient patient9 = new Patient("Sandy Thane", 
                new Date(2018, 03, 22, 8, 15, 0), "Doctor Reese", fatalRisk);
        Patient patient10 = new Patient("Jake Pill", 
                new Date(2020, 03, 22, 8, 15, 0), "Doctor Valeria", lowRisk);
        
        List<Patient> group = new ArrayList<>();
        group.add(patient1);
        group.add(patient2);
        group.add(patient3);
        group.add(patient4);
        group.add(patient5);
        group.add(patient6);
        group.add(patient7);
        group.add(patient8);
        group.add(patient9);
        group.add(patient10);
        
        ResearchGroup researchGroup1 = new ResearchGroup(group);
        
        
        List<Patient> patients = researchGroup1.getTotalPatients();
       
        researchGroup1.setStartTime(new Date(2021, 07, 19, 12, 0));
        researchGroup1.setEndTime(new Date(2021, 02, 24, 2, 0));
        
        
        for(Patient group1 : patients){
             System.out.println("\nName: " + group1.getName() + 
                     " | Date of Birth: " + group1.getDate() + 
                     " | Family Doctor: " + group1.getFamilyDoctor()
                     + " | Wrist Bands: " + group1.getTotalBands().toString()); 
        }
        
        System.out.println("\nThe research group waited for " + 
                researchGroup1.getTotalWaitTime(researchGroup1.getStartTime(), 
                        researchGroup1.getEndTime()) + " seconds.");
    }
}
