package hospital;

public class WristBands {
    
    private int barCode;
    private String info;
    
    public WristBands(int barCode, String info){
        
        setBarCode(barCode);
        setInfo(info);
    }
    
    public int getBarCode(){
        return barCode;
    }
    
    public void setBarCode(int barCode){
        this.barCode = barCode;
    }
    
    public String getInfo(){
        return info;
    }
    
    public void setInfo(String info){
        this.info = info; 
    }
    
    @Override
    public String toString(){
        return "Info: " + this.getInfo() + " | Bar code: " + this.getBarCode();
    }
}
